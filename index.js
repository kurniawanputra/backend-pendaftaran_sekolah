const express = require('express');
const app = express();
const bodyparser = require('body-parser');
const routes = require('./routes');
const expressValidator = require('express-validator');
const ekspressJwt = require('express-jwt');
const jwt = require('jsonwebtoken');

app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json());
app.use(expressValidator());


app.use('/v1', function(req, res, next){
	res.header("Access-Control-Allow-Origin", "*");
  	res.header("Access-Control-Allow-Headers", "Origin, Content-Type, Accept, Authorization, Content-Length, Date, X-Api-Version, x-access-token, X-Response-Time, X-PINGOTHER ");
  	next();
},routes);

app.listen(3500, function () {
  console.log('Example app listening on port 3500!')
});