var express = require('express');
var knex = require('knex');
var bodyparser = require('body-parser');
var db = require('./koneksi');
var siswa = require('./siswa');
let routes = express.Router();
var app = express();
var multer  = require('multer');
var fs = require('fs'); 
const path = require('path');
var ekspressJwt = require('express-jwt');
var jwt = require('jsonwebtoken');
var jwt_config = require('./middleware/auth');
var jwt_key	= require('./config/configAuth');
var Berita = require('./model/berita');
var SiswaModel = require('./model/siswa');
var UserModel = require('./model/user');
var FormatDate = require('./model/date');
var saranModel = require('./model/saran');
const joi = require('joi');
require('dotenv').config()

let auth = function(req, res, next) {
	console.log(req.header('Authorization'))
	next();
}

//app.use(ekspressJwt({ secret: 'kurniawan'}).unless({ path: ['/v1/checkLogin']}));
app.use(ekspressJwt({ secret: jwt_key}).unless({ path: ['/v1/checkLogin']}));

routes.get('/datadetail', jwt_config, function(req, res){
	var table ='mastersiswa';
	console.log("table "+ table);
	siswa.SelectSiswa(table, (error, data) => {
		if (error) res,status(400).json({'message' : 'error', 'error' : error});
		console.log(data);
		res.status(200).json({'message' : 'success','data' : data});
	})
});

routes.get('/datasiswa', function(req, res){
	var table ='mastersiswa';
	console.log("table "+ table);
	siswa.SelectSiswa(table, (error, data) => {
		if (error) res,status(400).json({'message' : 'error', 'error' : error});
		console.log(data);
		res.status(200).json({'message' : 'success','data' : data});
	})
});

routes.get('/list', function(req, res) {
	var table ='mastersiswa';
	console.log("table"+ table);
	siswa.List(table, (error, data) => {
		if (error) res.status(400).json({'message' : 'error','error' : error});
		console.log(data);
		res.status(200).json({'message' : 'success','data' : data});
	})
});

routes.get('/listUser', function(req,res) {
	var table ='user';
	console.log("table"+ table);
	siswa.listUser(table, (error, data) => {
		if (error) res.status(400).json({'message' : 'error','error' : error});
		console.log(data);
		res.status(200).json({'message' : 'success','data' : data});
	})
});

routes.get('/listsearch/:id', function(req, res) {
	var id = req.params.id;

	siswa.searchId(id, (error, data) => {
		if (error) res,status(400).json({'message' : 'error','error' : error});
		console.log(data);
		res.status(200).json({'message' : 'success','data' : data});
	})
});

routes.get('/listsearchuser/:id_user', function(req, res) {
	var id_user = req.params.id_user;
	siswa.searchUser(id_user, (error, data) => {
		if (error) res.status(400).json({'message' : 'error','error' : error});
		console.log(data);
		res.status(200).json({'message' : 'success','data' : data});
	})
})

routes.get('/listsearchsiswa/:id_siswa', function(req,res) {
	var id_siswa = req.params.id_siswa;
	SiswaModel.searchsiswa(id_siswa, (error, data) => {
		if (error) res.status(400).json({'message' : 'error','error' : error});
		console.log(data);
		res.status(200).json({'message' : 'success','data' : data});
	})
});

routes.get('/detailberita/:id_berita', function(req, res) {
	var id_berita = req.params.id_berita;
	Berita.searchBerita(id_berita, (error, data) => {
		if (error) res.status(400).json({'message' : 'error','error' : error});
		console.log(data);
		res.status(200).json({'message' : 'success','data' : data});
	})
})

routes.get('/findberita/:id_berita', function(req, res) {
	var id_berita = req.params.id_berita;
	Berita.searchBerita(id_berita, (error, data) => {
		if (error) res.status(400).json({'message' : 'error','error' : error});
		console.log(data);
		res.status(200).json({'message' : 'success','data' : data});
	})
})

routes.get('/findsiswa/:keyword', function(req, res) {
	var keyword = req.params.keyword;
	SiswaModel.findSiswa(keyword, (error,data) => {
		if(error) res.status(400).json({'message' : 'error','error' : error});
		console.log(data);
		res.status(200).json({'message' : 'success','data' : data});
	})
})

routes.get('/findaccount/:keyword', function(req, res) {
	var keyword = req.params.keyword;
	UserModel.findAccount(keyword, (error,data) => {
		if(error) res.status(400).json({'message' : 'error','error' : error});
		console.log(data);
		res.status(200).json({'message' : 'success','data' : data});
	})
})

routes.get('/searchberita/:keyword', function(req, res) {
	var keyword = req.params.keyword;
	Berita.findBerita(keyword, (error,data) => {
		if(error) res.status(400).json({'message' : 'error','error' : error});
		console.log(data);
		res.status(200).json({'message' : 'success','data' : data});
	})
})


routes.get('/listberitasecond', function(req, res) {
	Berita.headNewsSecond((error, data) => {
		if(error) res.status(400).json({'message' : 'error','error' : error});
		console.log(data);
		res.status(200).json({'message' : 'success','data' : data});
	})
})


routes.get('/listaccountusername', function(req, res) {
	UserModel.ListUser((error, data) => {
		if(error) res.status(400).json({'message' : 'error','error' : error});
		console.log(data);
		res.status(200).json({'message' : 'success', 'data' : data});
	})
})

routes.get('/listsiswa/:page', function(req, res) {
	const limit = 10;
	const offset = (limit * parseInt(req.params.page)) - limit; // 0, 10, 20
	SiswaModel.viewListSiswa((error, data, total) => {
		if (error) res.status(400).json({'message' : 'error','error' : error});
		res.status(200).json({'message' : 'success', 'data' : data, page: parseInt(req.params.page), limit: limit, total});
	}, limit, offset);
})

routes.get('/listberita/:page', function(req, res) {
	const limit = 10;
	const offset = (limit * parseInt(req.params.page)) - limit; // 0, 10, 20
	Berita.listBerita((error, data, total) => {
		if (error) res.status(400).json({'message' : 'error','error' : error});
		res.status(200).json({'message' : 'success', 'data' : data, page: parseInt(req.params.page), limit: limit, total});
	}, limit, offset);
})

routes.get('/listberitaheadline/:page', function(req, res) {
	const limit = 10;
	const offset = (limit * parseInt(req.params.page)) - limit; // 0, 10, 20
	Berita.viewBeritaHead((error, data, total) => {
		if (error) res.status(400).json({'message' : 'error','error' : error});
		res.status(200).json({'message' : 'success', 'data' : data, page: parseInt(req.params.page), limit: limit, total});
	}, limit, offset);
})

routes.get('/headnews', function(req, res) {
	Berita.headNews((error, data) => {
		if(error) res.status(400).json({'message' : 'error','error' : error});
		console.log(data);
		res.status(200).json({'message' : 'success','data' : data});
	})
})

routes.post('/createsiswa', function(req, res) {
	var nama = req.body.nama;
	var alamat = req.body.alamat;
	var errors = req.validationErrors();
	if(errors) {
		res.status(400).json({'message' : 'error 400','error' : errors});
	}else {
		siswa
			.InsertSiswa(nama, alamat)
			.then(result => {
				res.status(200).json({
					"status" : "success",
					"message" : "Success to insert data",
					"data" : req.body
				})
			})
			.catch(error => {
				res.status(400).json({
					"status" : "error",
					"error" : error
				})

			})
	}

});

routes.post('/createsiswabaru', function(req, res) {
	var nama = req.body.nama;
	var alamat = req.body.alamat;
	var errors = req.validationErrors();
	if(errors) {
		res.status(400).json({'message' : 'error 400','error' : errors});
	}else {
		siswa
			.InsertSiswaLagi(req.body)
			.then(result => {
				res.status(200).json({
					"status" : "success",
					"message" : "Success to insert data",
					"data" : req.body
				})
			})
			.catch(error => {
				res.status(400).json({
					"status" : "error",
					"error" : error
				})
			})
	}
});

routes.post('/addsaran', function(req,res) {
	var nama_saran = req.body.nama_saran;
	var email = req.body.email;
	var isi = req.body.isi;
	var errors = req.validationErrors();
	if(errors) {
		res.status(400).json({'message' : 'error 400','error' : errors});
	} else {
		saranModel
			.addSaran(req.body)
			.then(result => {
				res.status(200).json({
					"status" : "success",
					"message" : "success insert data",
					"data" : req.body
				})
			})
			.catch(error => {
				res.status(400).json({
					"status" : "error",
					"error" : error
				})
			})
	}

});

routes.post('/createuser', function(req, res) {
	var password = req.body.password;
	var username = req.body.username;
	var errors = req.validationErrors();
	if (errors) {
		res.status(400).json({'message' : 'error 400','error' : errors});
	}else {
		siswa
			.Createuser(req.body)
			.then(result => {
				res.status(200).json({
					"status" : "success",
					"message" : "Success to insert data",
					"data" : req.body
				})
			})
			.catch(error => {
				res.status(400).json({
					"status" : "error",
					"error" : error
				})
			})
	}
});

routes.post('/updateuser', function(req, res) {
	var id_user = req.body.id_user;
	var password = req.body.password;
	var username = req.body.username;
	var errors = req.validationErrors();
	if (errors) {
		res.status(400).json({'message' : 'error 400','error' : errors});
	} else {
		siswa
			.updateUser(req.body)
			.then(result => {
				res.status(200).json({
					"status" : "success",
					"message" : "success update data",
					"data" : req.body
				})
			})
			.catch(error => {
				res.status(400).json({
					"status" : "error",
					"error" : error
				})
			})
	}
});

routes.post('/updatesiswa', function(req,res) {
	var id_siswa = req.body.id_siswa;
	var nama_siswa = req.body.nama_siswa;
	var alamat = req.body.alamat;
	var email = req.body.email;
	var hp = req.body.email;
	var gender = req.body.gender;
	var ttl = req.body.ttl;
	var agama = req.body.agama;
	var sekolah = req.body.sekolah;
	var nama_ayah = req.body.nama_ayah;
	var nama_ibu = req.body.nama_ibu;
	var kerja_ayah = req.body.kerja_ayah;
	var errors = req.validationErrors();
	if (errors) {
		res.status(400).json({'message' : 'error 400','error' : errors});
	} else {
		SiswaModel
			.updateSiswa(req.body)
			.then(result => {
				res.status(200).json({
					"status" : "success",
					"message" : "success update data",
					"data" : req.body
				})
			})
			.catch(error => {
				res.status(400).json({
					"status" : "error",
					"error" : error
				})
			})
	}
});

routes.post('/deletesiswa', function(req,res) {
	var id_siswa = req.body.id_siswa;
	var errors = req.validationErrors();
	if(errors) {
		res.status(400).json({'message' : 'error 400','error' : errors});
	} else {
		SiswaModel
			.deleteSiswa(req.body)
			.then(result => {
				res.status(200).json({
					"status" : "success",
					"message" : "success delete data",
					"data" : req.body
				})
			})
			.catch(error => {
				res.status(400).json({
					"status" : "error",
					"error" : error,
					"data" :req.body
				})
			})
	}
});

routes.post('/deletuser', function(req,res) {
	var id_user = req.body.id_user;
	var errors = req.validationErrors();
	if(errors) {
		res.status(400).json({'message' : 'error 400','error' : errors});
	} else {
		UserModel
			.deleteUser(req.body)
			.then(result => {
				res.status(200).json({
					"status" : "success",
					"message" : "success delete data",
					"data" : req.body
				})
			})
			.catch(error => {
				res.status(400).json({
					"status" : "error",
					"error" : error,
					"data" :req.body
				})
			})
	}
});

routes.post('/deleteberita', function(req,res) {
	
	const objectValidation = joi.object().keys({
		id_berita : joi.number().required()
	});

	joi.validate({ id_berita : req.body.id_berita}, (error, result) => {
		if(error) {
			res.status(400).json({'message' : 'Terjadi Kesalahan','error' : error.message});
		} else {
			Berita
				.deleteBerita(result)
				.then(result => {
					res.status(200).json({
						"message" : "success delete data",
						"status" : "success",
						"data" : req.body
					})
				})
				.catch(err => {
					res.status(400).json({
						"status" : "error",
						"error" : err.message,
						"data" : req.body
					})
				})
		}
	})
});

routes.post('/checkLogin', function(req, res) {
	//var username = req.checkBody("username","required").notEmpty();
	//var password = req.checkBody("password","required").notEmpty();
	var errors = req.validationErrors();
	if (errors) {
		res.status(400).json({'message' : 'error 400','error' : error});
	} else {
		siswa
			.cekLogin(req.body)
			.then(result => {
				//var token = jwt.sign({ username: req.body}, 'kurniawan');
				 var token = jwt.sign(req.body, jwt_key.secret);
				//console.log(jwt_key.secret)
				res.status(200).json({
					"status" : "success",
					"message" : "success login",
					"token" : token
				})
			})
			.catch(error => {
				res.status(400).json({
					"status" : "login gagal",
					"error" : error
				})
			})
	}
});

routes.post('/addsiswa', function(req, res) {
	var nama_siswa = req.body.nama_siswa;
	var alamat = req.body.alamat;
	var email = req.body.email;
	var hp = req.body.hp;
	var gender = req.body.gender;
	var ttl = req.body.ttl;
	var agama = req.body.agama;
	var sekolah = req.body.sekolah;
	var nama_ayah = req.body.nama_ayah;
	var nama_ibu = req.body.nama_ibu;
	var kerja_ayah = req.body.kerja_ayah;
	var errors = req.validationErrors();
	if (errors) {
		res.status(400).json({'message' : 'error 400', 'error' : errors});
	} else {
		siswa
			.Addsiswa(req.body)
			.then(result => {
				res.status(200).json({
					"status" : "success",
					"message" : "success insert to data",
					"data" : req.body
				})
			})
			.catch(error => {
				res.status(400).json({
					"status" : "error",
					"error" : error
				})
			})
	}
});


var storage = multer.diskStorage({
	destination: function (request, file, callback) {
		callback(null, './photo');
	},
	filename: function (request, file, callback) {
		callback(null, file.originalname);
	}
});
var upload = multer({storage: storage}).single('file');

routes.post('/upload', (request,response) => {    
	upload(request, response, (err) => {         
		if(err) {             
			console.log('Error Occured');             
				response.status(400).json({                 
					status : "error",                 
					message :"File failed to upload",                 
					error : err             
				});         
			} else {
			siswa
				.addImage({
					nama : request.file.originalname,
					image : '/image/' + request.file.filename
				})         
					response.status(200).json({             
					status : "success",             
					message :"File uploaded",             
					images : request.file     
				});
			}     
	}); 
});

routes.get('/image/:image', function(req, res){
	let pathFile = path.resolve('./photo/' + req.params.image)

	res.sendFile(pathFile);
});

routes.post('/TambahBerita', (request,response) => { 
	upload(request, response, (err) => {         
		if(err) {             
			console.log('Error Occured');             
				response.status(400).json({                 
					status : "error",                 
					message :"File failed to upload",                 
					error : err             
				});         
			} else {
			Berita
				.addBerita({
					judul_berita : request.body.judul_berita,
					deskripsi_berita : request.body.deskripsi_berita,
					isi_berita : request.body.isi_berita,
					// tanggal : date.toJSON(),
					image_berita : process.env.REACT_APP_URL + '/v1/image/' + request.file.filename
				})         
					response.status(200).json({             
					status : "success",             
					message :"File uploaded",             
					images : request.file.filename,
					data : request.body     
				});
			}     
	}); 
});

routes.post('/UpdateBerita', (request,response) => {
	upload(request, response, (err) => {
		if(err) {
			console.log('Error Occured');
				response.status(400).json({
					status : "error",
					message : "Failed Upload",
					error : err
				});
		} else {
			Berita
				.updateBerita({
					id_berita : request.body.id_berita,
					judul_berita : request.body.judul_berita,
					deskripsi_berita : request.body.deskripsi_berita,
					isi_berita : request.body.isi_berita,
					// tanggal : date.toJSON(),
					image_berita : process.env.REACT_APP_URL + '/v1/image/' + request.file.filename
				})
					response.status(200).json({
						status : "success",
						message : "File upload is success",
						images : request.file.filename,
						data : request.body
				});
		}

	});
});

var date = new Date();




//routes.post('/addBerita', jwt_config, Berita.addBerita);

module.exports = routes