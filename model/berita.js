var express = require('express');
var app = express();
var knex = require('knex');
var bodyparser = require('body-parser');
var db = require('../koneksi');

function addBerita(data) {
	return new Promise((resolve, reject) => {
		db('berita')
			.insert({
				"judul_berita": data.judul_berita,
				"deskripsi_berita": data.deskripsi_berita,
				"isi_berita": data.isi_berita,
				// "tanggal" : data.tanggal,
				// "tanggal" : "2017-06-01 18:30",
				"image_berita": data.image_berita
			})
			.then(result => {
				resolve(result)
			})
			.catch(error => {
				reject(error)
			})
	})
}

function viewBeritaHead(callback, limit, offset) {
	db
		.select('id_berita', 'judul_berita', 'deskripsi_berita', 'isi_berita', 'image_berita')
		.from('berita')
		.then(function (res) {
			db
				.select('id_berita', 'judul_berita', 'deskripsi_berita', 'isi_berita', 'image_berita')
				.from('berita')
				.limit(limit)
				.offset(offset)
				.then(function (result) {
					callback(null, result, res.length);
				})
				.catch(function (error) {
					callback(null, error);
				})
		})
		.catch(function (error) {
			callback(null, error);
		})
}

function listBerita(callback, limit, offset) {
	db
		.select('id_berita', 'judul_berita', 'deskripsi_berita', 'isi_berita', 'image_berita')
		.from('berita')
		.then(function (res) {
			db
				.select('id_berita', 'judul_berita', 'deskripsi_berita', 'isi_berita', 'image_berita')
				.from('berita')
				.limit(limit)
				.offset(offset)
				.then(function (result) {
					callback(null, result, res.length);
				})
				.catch(function (error) {
					callback(null, error);
				})
		})
		.catch(function (error) {
			callback(null, error);
		})
}

function pagingPage(callback) {
	db
		.select('id_berita', 'judul_berita', 'deskripsi_berita', 'isi_berita', 'image_berita')
		.from('berita')
		.limit(10)
		.then(function (result) {
			callback(null, result);
		})
		.catch(function (error) {
			callback(null, error);
		})
}
function headNews(callback) {
	db
		.select('id_berita', 'judul_berita', 'deskripsi_berita', 'isi_berita', 'image_berita')
		.from('berita')
		.limit(3)
		.orderBy('id_berita', 'desc')
		.then(function (result) {
			callback(null, result);
		})
		.catch(function (error) {
			callback(null, error);
		})
}

function headNewsSecond(callback) {
	db
		.select('id_berita', 'judul_berita', 'deskripsi_berita', 'isi_berita', 'image_berita')
		.from('berita')
		.limit(5)
		.orderBy('id_berita', 'desc')
		.then(function (result) {
			callback(null, result);
		})
		.catch(function (error) {
			callback(null, error);
		})
}

function searchBerita(id_berita, callback) {
	db
		.select('id_berita', 'judul_berita', 'deskripsi_berita', 'isi_berita', 'image_berita')
		.from('berita')
		.where('id_berita', id_berita)
		.then(function (result) {
			callback(null, result);
		})
		.catch(function (error) {
			callback(null, error);
		})
}

function updateBerita(data) {
	return new Promise((resolve, reject) => {
		db('berita')
			.where('id_berita', data.id_berita)
			.update({
				"judul_berita": data.judul_berita,
				"deskripsi_berita": data.deskripsi_berita,
				"isi_berita": data.isi_berita,
				"image_berita": data.image_berita
			})
			.then(result => {
				resolve(result)
			})
			.catch(error => {
				reject(error)
			})
	})
}

function deleteBerita(data) {
	return new Promise((resolve, reject) => {
		db('berita')
			.del()
			.where('id_berita', data.id_berita)
			.then(result => {
				resolve(result)
			})
			.catch(error => {
				reject(error)
			})
	})
}

function findBerita(keyword, callback) {
	db
		.select('judul_berita', 'deskripsi_berita', 'isi_berita')
		.from('berita')
		.where('judul_berita', 'like', '%' + keyword + '%')
		.orWhere('deskripsi_berita', 'like', '%' + keyword + '%')
		.orWhere('isi_berita', 'like', '%' + keyword + '%')
		.then(function (result) {
			callback(null, result);
		})
		.catch(function (error) {
			callback(error, null);
		})
}

module.exports = {
	addBerita: addBerita,
	viewBeritaHead: viewBeritaHead,
	searchBerita: searchBerita,
	headNews: headNews,
	headNewsSecond: headNewsSecond,
	listBerita: listBerita,
	updateBerita: updateBerita,
	deleteBerita: deleteBerita,
	findBerita: findBerita,
	pagingPage: pagingPage
};