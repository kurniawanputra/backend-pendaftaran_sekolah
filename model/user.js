var express = require('express');
var app = express();
var knex = require('knex');
var bodyparser = require('body-parser');
var db = require('../koneksi');

function ListUser(callback) {
	db
		.select('id_user','username','password')
		.from('user')
		.then(function(result) {
			callback(null, result);
		})
		.catch(function(error) {
			callback(null, error);
		})
}

function findAccount(keyword, callback) {
	db
		.select('id_user','username', 'password')
		.from('user')
		.where('username', 'like', '%'+keyword+'%')
		.then(function(result) {
			callback(null, result);
		})
		.catch(function(error) {
			callback(null, error);
		})
}

function deleteUser(data) {
	return new Promise((resolve,reject) => {
		db('user')
		.del()
		.where('id_user', data.id_user)
		.then(result => {
			resolve(result)
		})
		.catch(error => {
			reject(error)
		})
	})
}

module.exports = {
	ListUser: ListUser,
	findAccount: findAccount,
	deleteUser: deleteUser
}