var express = require('express');
var app = express();
var knex = require('knex');
var bodyparser = require('body-parser');
var db = require('../koneksi');


function addSaran(data) {
	return new Promise((resolve, reject) => {
		db('saran')
			.insert({
				"nama_saran" : data.nama_saran,
				"email" : data.email,
				"isi" : data.isi
			})
			.then(result => {
				resolve(result)
			})
			.catch(error => {
				reject(error)
			})
	})
}

module.exports = {
	addSaran: addSaran
};