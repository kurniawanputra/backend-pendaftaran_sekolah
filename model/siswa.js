var express = require('express');
var app = express();
var knex = require('knex');
var bodyparser = require('body-parser');
var db = require('../koneksi');


function viewListSiswa(callback, limit, offset) {
	db
		.select('id_siswa','nama_siswa','alamat','email','hp','gender','ttl','agama','sekolah','nama_ayah','nama_ibu','kerja_ayah')
		.from('siswa')
		.then(function (res) {
			db
				.select('id_siswa','nama_siswa','alamat','email','hp','gender','ttl','agama','sekolah','nama_ayah','nama_ibu','kerja_ayah')
				.from('siswa')
				.limit(limit)
				.offset(offset)
				.then(function (result) {
					callback(null, result, res.length);
				})
				.catch(function (error) {
					callback(null, error);
				})
		})
		.catch(function (error) {
			callback(null, error);
		})
}

function searchsiswa(id_siswa, callback) {
	db
		.select('id_siswa','nama_siswa','alamat','email','hp','gender','ttl','agama','sekolah','nama_ayah','nama_ibu','kerja_ayah')
		.from('siswa')
		.where('id_siswa', id_siswa)
		.then(function(result) {
			callback(null, result);
		})
		.catch(function(error) {
			callback(error, null);
		})
}

function findSiswa(keyword, callback) {
	db
		.select('id_siswa','nama_siswa','alamat','email','hp','gender','ttl','agama','sekolah','nama_ayah','nama_ibu','kerja_ayah')
		.from('siswa')
		.where('nama_siswa', 'like', '%'+keyword+'%')
			.orWhere('alamat', 'like', '%'+keyword+'%')
			.orWhere('nama_ayah', 'like', '%'+keyword+'%')
			.orWhere('nama_ibu', 'like', '%'+keyword+'%')
		.then(function(result) {
			callback(null, result);
		})
		.catch(function(error) {
			callback(error, null);
		})
}

function deleteSiswa(data) {
	return new Promise((resolve,reject) => {
		db('siswa')
		.del()
		.where('id_siswa', data.id_siswa)
		.then(result => {
			resolve(result)
		})
		.catch(error => {
			reject(error)
		})
	})
}

function updateSiswa(data) {
	return new Promise((resolve,reject) => {
		db('siswa')
		.where('id_siswa', data.id_siswa)
		.update({
			"nama_siswa" : data.nama_siswa,
			"alamat" : data.alamat,
			"email" : data.email,
			"hp" : data.hp,
			"gender" : data.gender,
			"ttl" : data.ttl.toString("yyyyMMddHHmmss").replace(/T/, ' ').replace(/\..+/, ''),
			"agama" : data.agama,
			"sekolah" : data.sekolah,
			"nama_ayah" : data.nama_ayah,
			"nama_ibu" : data.nama_ibu,
			"kerja_ayah" : data.kerja_ayah
		})
		.then(result => {
			resolve(result)
		})
		.catch(error => {
			reject(error)
		})
	})
}

module.exports = {
	viewListSiswa: viewListSiswa,
	searchsiswa: searchsiswa,
	updateSiswa: updateSiswa,
	deleteSiswa: deleteSiswa,
	findSiswa: findSiswa
}