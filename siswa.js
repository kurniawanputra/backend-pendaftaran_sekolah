var express = require('express');
var app = express();
var knex = require('knex');
var bodyparser = require('body-parser');
var db = require('./koneksi');
var routes = express.Router();
var bcrypt = require('bcrypt-nodejs');
var multer  = require('multer');
var jwt_key	= require('./middleware/auth');


function SelectSiswa(table, callback) {
	db
		.select('nama', 'alamat')
		.from(table)
		.limit(1)
		.then(function(result) {
			callback(null, result);
		})
		.catch(function(error) {
			callback(error, null);
		})
}

function List(table,callback) {
	db
		.select('id','nama','alamat')
		.from(table)
		.then(function(result) {
			callback(null, result);
		})
		.catch(function(error) {
			callback(error, null);
		})
}

function listUser(table,callback) {
	db
		.select('id_user','username')
		.from(table)
		.then(function(result) {
			callback(null, result);
		})
		.catch(function(error) {
			callback(error, null);
		})
}

function searchId(id, callback) {
	db
		.select('id','nama','alamat')
		.from('mastersiswa')
		.where('id', id)
		.then(function(result) {
			callback(null, result);
		})
		.catch(function(error) {
			callback(error, null);
		})
}

function searchUser(id_user, callback) {
	db
		.select('id_user','username')
		.from('user')
		.where('id_user', id_user)
		.then(function(result) {
			callback(null, result);
		})
		.catch(function(error) {
			callback(error, null);
		})
}

function InsertSiswa(resolve, reject, data) {
	db('mastersiswa')
		.insert({
			"nama" : data.nama,
			"alamat" : data.alamat
		})
		.then(result => {
			resolve(result)
		})
		.catch(error => {
			reject(error)
		})
}

function InsertSiswaLagi(data) {
	return new Promise((resolve, reject) => {
		db('mastersiswa')
			.insert({
			"nama" : data.nama,
			"alamat" : data.alamat
		})
		.then(result => {
			resolve(result)
		})
		.catch(error => {
			reject(error)
		})
	})
}

function Createuser(data) {
	return new Promise((resolve, reject) => {
		bcrypt.hash(data.password, null, null, function(err, hash) {
    		if (err) {
    			reject(err)
    		} else {
    			db('user')
				.insert({
					"password" : hash,
					"username" : data.username
				})
				.then(result => {
					resolve(result)
				})
				.catch(error => {
					reject(error)
				})
    		}
		});
	})
}

function cekLogin(data) {
	return new Promise((resolve, reject) => {
		db('user')
				.select('username', 'password')
				.from('user')
				.where('username', data.username)
				.then(result => {
					if (bcrypt.compareSync(data.password, result[0].password)) {
						resolve(result)
					} else {
						reject('tidak ada user')
					}
				})
				.catch(error => {
					reject(error)
				})
	})
}


function updateUser(data) {
	return new Promise((resolve, reject) => {
		bcrypt.hash(data.password, null, null, function(err, hash) {
			if (err) {
				reject(err)
			} else {
				db('user')
				.where('id_user', data.id_user)
				.update({
					"password" : hash,
					"username" : data.username
				})
				.then(result => {
					resolve(result)
				})
				.catch(error => {
					reject(error)
				})
			}
		});
	})
}

function Addsiswa(data) {
	return new Promise((resolve, reject) => {
		db('siswa')
			.insert({
				"nama_siswa" : data.nama_siswa,
				"alamat" : data.alamat,
				"email" : data.email,
				"hp" : data.hp,
				"gender" : data.gender,
				"ttl" : data.ttl.toString("yyyyMMddHHmmss").replace(/T/, ' ').replace(/\..+/, ''),
				"agama" : data.agama,
				"sekolah" : data.sekolah,
				"nama_ayah" : data.nama_ayah,
				"nama_ibu" : data.nama_ibu,
				"kerja_ayah" : data.kerja_ayah
			})
			.then(result => {
				resolve(result)
			})
			.catch(error => {
				reject(error)
			})
	})
}


function addImage(data) {
	return new Promise((resolve, reject) => {
		db('gallery')
			.insert({
				"image_location" : data.image,
				"nama" : data.nama
			})
			.then(result => {
				resolve(result)
			})
			.catch(error => {
				reject(error)
			})
	})
}

module.exports = {
    SelectSiswa: SelectSiswa,
    InsertSiswa: InsertSiswa,
    InsertSiswaLagi: InsertSiswaLagi,
    Addsiswa: Addsiswa,
    List: List,
    searchId: searchId,
    Createuser: Createuser,
    listUser: listUser,
    searchUser: searchUser,
    updateUser: updateUser,
    addImage: addImage,
    cekLogin: cekLogin
};