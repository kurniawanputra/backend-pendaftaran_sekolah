FROM kurniawanputra/backend-pendaftaran_sekolah:base

ADD . /var/www/backend-pendaftaran_sekolah
WORKDIR /var/www/backend-pendaftaran_sekolah
RUN npm install
CMD ["node", "index.js"]