var jwt = require('jsonwebtoken');
var jwt_config = require('../config/configAuth');

module.exports = function (req, res, next) {
	var token = req.header('Authorization');
	if(token) {
		jwt.verify(token, jwt_config.secret, function(err, decoded) {
			if (err) {
				return res.send({ success: false, message: 'Failed to authenticate token'});
			} else {
				req.decoded = decoded;
				res.header('Cache-Control','private, no-cache, no-store, must-revalidate');
				res.header('Expires', '-1');
				res.header('Pragma','no-cache');

				next();
			}
		});
	} else {
		res.send(403, {
			message : "No Token Provided"
		});
	}
};

//module.exports = { jwt_key: jwt_key }